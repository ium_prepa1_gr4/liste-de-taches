# Gestionnaire de Tâches en Ligne de Commande

Ce script Python est un gestionnaire de tâches simple qui permet à l'utilisateur de créer, afficher, marquer comme terminées et supprimer des tâches stockées dans un fichier JSON.

## Fonctionnalités

- **Ajouter une tâche** : Permet à l'utilisateur d'entrer une description pour une nouvelle tâche qui sera ajoutée avec l'état "en cours".
- **Afficher les tâches** : Affiche toutes les tâches enregistrées avec leur ID, description et état.
- **Marquer une tâche comme terminée** : Permet de changer l'état d'une tâche spécifique à "terminée" en utilisant son ID.
- **Supprimer une tâche** : Supprime une tâche spécifique en utilisant son ID.
- **Quitter** : Ferme l'application.

## Comment ça marche ?

1. **Charger les tâches** : Au démarrage, l'application tente de charger les tâches existantes à partir d'un fichier `taches.json`. Si le fichier n'existe pas, une liste vide est retournée.

2. **Menu principal** : L'utilisateur est accueilli par un menu principal qui lui présente les différentes actions possibles. L'utilisateur peut choisir une option en entrant le numéro correspondant.

3. **Exécution des actions** : Selon le choix de l'utilisateur, une fonction correspondante est appelée pour exécuter l'action demandée.

4. **Enregistrement des tâches** : Après chaque modification (ajout, suppression, mise à jour), les tâches sont enregistrées dans le fichier `taches.json`.

## Structure du Code

- `charger_taches()` : Charge les tâches depuis le fichier JSON ou retourne une liste vide si le fichier n'existe pas.
- `afficher_taches()` : Affiche la liste des tâches chargées.
- `enregistrer_taches(taches)` : Enregistre la liste des tâches dans le fichier JSON.
- `marquer_tache_terminee(id_tache)` : Marque une tâche spécifiée par son ID comme terminée.
- `supprimer_tache(id_tache)` : Supprime une tâche spécifiée par son ID.
- `ajouter_tache()` : Ajoute une nouvelle tâche à la liste.
- `menu()` : Affiche le menu principal et gère les entrées de l'utilisateur.

## Utilisation

Pour utiliser l'application, exécutez le script dans un terminal. Suivez les instructions à l'écran pour gérer vos tâches.

